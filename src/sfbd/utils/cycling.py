import time
from epics import caget, caput
from threading import Thread
from PyQt5.QtCore import  QObject,pyqtSignal
   

class Cycle(QObject):

    sigcycleprogress  = pyqtSignal(int, int)  # signal for increment
    sigcyclefinish = pyqtSignal()       # signal for termination

    def __init__(self):
        QObject.__init__(self)
        self.maxcycle = -1
        self.maxmagnet  = None

    def start(self,maglist=[], excludeDipoles = True):
        self.maxcycle=-1
        self.maxmagnet = None
        for mag in maglist:
            if excludeDipoles and 'MBND' in mag:
                continue
            val = caget(mag+':CYCLE-PROGFULL',timeout=1.0)
            if val:
                caput(mag+':CYCLE',2)
                if val > self.maxcycle:
                    self.maxcycle = val
                    self.maxmagnet = mag
        if not self.maxmagnet:
            return
        Thread(target=self.tcycle).start()

    def tcycle(self):
        waiting=True
        while(waiting):
            time.sleep(1.0)
            val = caget(self.maxmagnet+':CYCLE-PROGPART')
            if val:
                self.sigcycleprogress.emit(val,self.maxcycle)
                if val >= self.maxcycle:
                    waiting = False
            else:
                waiting = False
        self.sigcyclefinish.emit()

